package test.GL.java.tp1;

import main.GL.java.tp1.Banque;
import main.GL.java.tp1.Compte;
import main.GL.java.tp1.CompteEpargne;
import main.GL.java.tp1.exceptions.CompteInexistantException;
import main.GL.java.tp1.exceptions.DebitRefuseException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BanqueTest {
    Banque banque;

    @Before
    public void init() {
        banque = new Banque();
    }

    @Test
    public void constructorTest() {
        assertEquals(0, banque.getTotalCompteSoldes(), 0.1);
    }

    @Test
    public void ouvertureCompte() {
        assertTrue(banque.comptes.size() == 0);
        banque.ouvrirCompte(new Compte());
        assertTrue(banque.comptes.size() == 1);
        banque.ouvrirCompte(new CompteEpargne());
        assertTrue(banque.comptes.size() == 2);
    }

    @Test
    public void creditDebitTest() throws DebitRefuseException, CompteInexistantException {
        Compte c = new Compte();
        banque.ouvrirCompte(c);
        assertEquals(0, banque.getTotalCompteSoldes(), 0.1);
        banque.credite(0, 100);
        assertEquals(100, banque.getTotalCompteSoldes(), 0.1);
        assertEquals(100, c.getSolde(), 0.1);
        assertEquals(100, c.getCredit(), 0.1);
        banque.debite(0, 50);
        assertEquals(50, banque.getTotalCompteSoldes(), 0.1);
        assertEquals(50, c.getSolde(), 0.1);
        assertEquals(50, c.getDebit(), 0.1);
    }

    @Test(expected = CompteInexistantException.class)
    public void compteInexistantTest() throws CompteInexistantException {
        banque.credite(42, 50000);
    }

    @Test
    public void virementTest() throws CompteInexistantException, DebitRefuseException {
        Compte source = new Compte();
        Compte destination = new Compte();
        banque.ouvrirCompte(source);
        banque.ouvrirCompte(destination);
        banque.credite(0, 1000);
        banque.virement(0, 1, 500);
        assertEquals(1000, banque.getTotalCompteSoldes(), 0.1);
        assertEquals(500, source.getSolde(), 0.1);
        assertEquals(500, destination.getSolde(), 0.1);
    }

    @Test
    public void virementEpargneTest() throws CompteInexistantException, DebitRefuseException {
        Compte source = new Compte();
        Compte destination = new CompteEpargne();
        banque.ouvrirCompte(source);
        banque.ouvrirCompte(destination);
        banque.credite(0, 1000);
        banque.virement(0, 1, 500);
        assertEquals(1000, banque.getTotalCompteSoldes(), 0.1);
        assertEquals(500, source.getSolde(), 0.1);
        assertEquals(500, destination.getSolde(), 0.1);
    }

    @Test(expected = CompteInexistantException.class)
    public void virementExceptionTest() throws CompteInexistantException, DebitRefuseException {
        banque.virement(0, 1, 500);
    }
}
