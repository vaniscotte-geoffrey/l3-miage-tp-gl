package test.GL.java.tp1;

import main.GL.java.tp1.Compte;
import static org.junit.Assert.*;

import main.GL.java.tp1.exceptions.DebitRefuseException;
import org.junit.Test;
import org.junit.Before;

import java.util.ArrayList;
import java.util.List;


public class CreditDebitTest {
    Compte compte;

    @Before
    public void initCompte(){
        compte = new Compte();
    }

    @Test
    public void creditTest(){
        compte.credite(50);
        assertEquals(50, compte.getCredit(), 0.1);
        compte.credite(10);
        assertEquals(60, compte.getCredit(), 0.1);
    }

    @Test
    public void debitTest() throws DebitRefuseException {
        compte.debite(50);
        assertEquals(50, compte.getDebit(), 0.1);
        compte.debite(10);
        assertEquals(60, compte.getDebit(), 0.1);
    }

    @Test
    public void creditNegativeTest(){
        compte.credite(-50);
        assertEquals(0, compte.getCredit(), 0.1);
        compte.credite(-5);
        assertEquals(0, compte.getCredit(), 0.1);
        compte.credite(-0.1);
        assertEquals(0, compte.getCredit(), 0.1);
    }

    @Test
    public void creditZeroTest(){
        compte.credite(0);
        assertEquals(0, compte.getCredit(), 0.1);
        compte.credite(5);
        assertEquals(5, compte.getCredit(), 0.1);
        compte.credite(0);
        assertEquals(5, compte.getCredit(), 0.1);
    }

    @Test
    public void creditTropGrandTest() throws DebitRefuseException {
        compte.credite(100001);
        assertEquals(0, compte.getSolde(), 0.1);
        compte.credite(100000);
        assertEquals(100000, compte.getSolde(), 0.1);
        compte.credite(500);
        assertEquals(100500, compte.getSolde(), 0.1);
        compte.debite(100001);
        assertEquals(100500, compte.getSolde(), 0.1);
        compte.debite(100000);
        assertEquals(500, compte.getSolde(), 0.1);
    }

    @Test
    public void debitTestNegative() throws DebitRefuseException {
        compte.debite(-50);
        assertEquals(0, compte.getDebit(), 0.1);
        compte.debite(-5);
        assertEquals(0, compte.getDebit(), 0.1);
        compte.debite(-0.1);
        assertEquals(0, compte.getDebit(), 0.1);
    }

    @Test
    public void soldeTest() throws DebitRefuseException {
        assertEquals(0, compte.getSolde(), 0.1);
        compte.credite(100);
        assertEquals(100, compte.getSolde(), 0.1);
        compte.debite(20);
        assertEquals(80, compte.getSolde(), 0.1);
        compte.debite(50);
        assertEquals(30, compte.getSolde(), 0.1);
        compte.credite(10);
        assertEquals(40, compte.getSolde(), 0.1);
    }

    @Test
    public void creditHistoryTest() {
        List<Double> creditHistory = initWithZero(compte.getCreditHistory().size());
        assertArrayEquals(creditHistory.toArray(), compte.getCreditHistory().toArray());
        compte.credite(50);
        creditHistory.set(0, (double) 50);
        assertArrayEquals(creditHistory.toArray(), compte.getCreditHistory().toArray());
        double total = 50;
        for(int i = 1; i < creditHistory.size(); i++){
            compte.credite(i);
            creditHistory.set(i, (double) i);
            total += i;
        }
        compte.credite(1);
        creditHistory = initWithZero(compte.getCreditHistory().size());
        creditHistory.set(0, total);
        creditHistory.set(1, (double) 1);
        assertArrayEquals(creditHistory.toArray(), compte.getCreditHistory().toArray());
    }

    private List<Double> initWithZero(int length) {
        List<Double> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add((double) 0);
        }
        return list;
    }
}
