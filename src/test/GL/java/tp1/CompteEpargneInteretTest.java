package test.GL.java.tp1;

import main.GL.java.tp1.CompteEpargne;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompteEpargneInteretTest {

    CompteEpargne compte;

    @Before
    public void init() {
        compte = new CompteEpargne();
    }

    @Test
    public void interetTest() {
        compte.credite(100);
        assertEquals(100, compte.getCredit(), 0.001);
        assertEquals(100, compte.getSolde(), 0.001);
        compte.echeance();
        double ajoutInteret = compte.getSolde() * compte.getInteret() + compte.getSolde();
        assertEquals(100+100*compte.getInteret(), compte.getCredit(), 0.001);
        assertEquals(100+100*compte.getInteret(), compte.getSolde(), 0.001);
    }

}
