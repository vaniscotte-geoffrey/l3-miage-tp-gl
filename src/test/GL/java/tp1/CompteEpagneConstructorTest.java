package test.GL.java.tp1;

import static org.junit.Assert.*;

import main.GL.java.tp1.Compte;
import main.GL.java.tp1.CompteEpargne;
import org.junit.Before;
import org.junit.Test;

public class CompteEpagneConstructorTest {
    Compte compte;

    @Before
    public void init() {
        compte = new CompteEpargne();
    }

    @Test
    public void zeroCreditDebitTest(){
        assertEquals(0, compte.getDebit(), 0.001);
        assertEquals(0, compte.getCredit(), 0.001);
        assertEquals(0, compte.getSolde(), 0.001);
    }
}
