package test.GL.java.tp1;

import main.GL.java.tp1.Compte;
import main.GL.java.tp1.CompteEpargne;
import main.GL.java.tp1.exceptions.DebitRefuseException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompteEpagneCreditDebitTest {
    CompteEpargne compte;

    @Before
    public void init() {
        compte = new CompteEpargne();
    }

    @Test
    public void creditTest() {
        compte.credite(500);
        assertEquals(500, compte.getCredit(), 0.001);
        assertEquals(500, compte.getSolde(), 0.001);
        compte.credite(500);
        assertEquals(1000, compte.getCredit(), 0.001);
        assertEquals(1000, compte.getSolde(), 0.001);
    }

    /*@Test
    public void debitTest() {
        compte.credite(1000);
        assertEquals(1000, compte.getCredit(), 0.001);
        assertEquals(1000, compte.getSolde(), 0.001);
        compte.debite(1001);
        assertEquals(0, compte.getDebit(), 0.001);
        assertEquals(1000, compte.getSolde(), 0.001);
        compte.debite(1000);
        assertEquals(1000, compte.getDebit(), 0.001);
        assertEquals(0, compte.getSolde(), 0.001);
    }*/

    @Test(expected = DebitRefuseException.class)
    public void debitExceptionTest() throws DebitRefuseException {
        compte.credite(1000);
        assertEquals(1000, compte.getCredit(), 0.001);
        assertEquals(1000, compte.getSolde(), 0.001);
        compte.debite(1001);
    }
}
