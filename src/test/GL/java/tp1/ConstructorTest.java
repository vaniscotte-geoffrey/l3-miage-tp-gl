package test.GL.java.tp1;


import main.GL.java.tp1.Compte;
import static org.junit.Assert.*;
import org.junit.Test;

public class ConstructorTest {
    @Test
    public void zeroCreditDebitTest(){
        Compte compte = new Compte();
        assertEquals(0, compte.getDebit(), 0.001);
        assertEquals(0, compte.getCredit(), 0.001);
    }
}
