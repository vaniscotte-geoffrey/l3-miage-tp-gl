package main.GL.java.tp1;

import main.GL.java.tp1.exceptions.DebitRefuseException;

import java.util.ArrayList;
import java.util.List;

public class Compte {
    private final int HISTORY_SIZE = 10;
    private List<Double> creditHistory, debitHistory;
    private int currentIndex;

    public Compte() {
        creditHistory = new ArrayList<>();
        debitHistory = new ArrayList<>();
        initWithZero(creditHistory, HISTORY_SIZE);
        initWithZero(debitHistory,HISTORY_SIZE);
        currentIndex = 0;
    }

    private List<Double> initWithZero(List<Double> list, int length) {
        list.clear();
        for (int i = 0; i < length; i++) {
            list.add((double) 0);
        }
        return list;
    }

    public double getDebit() {
        return getSum(debitHistory);
    }

    public double getCredit() {
        return getSum(creditHistory);
    }

    private double getSum(List<Double> list) {
        return list.stream().mapToDouble(Double::doubleValue).sum();
    }

    public void credite (double credit){
        addTransactionToList(creditHistory, credit);
    }

    public void debite(double debit) throws DebitRefuseException {
        addTransactionToList(debitHistory, debit);
    }

    private void addTransactionToList(List<Double> list, double value) {
        if(value < 0 || value > 100000) return;
        if(currentIndex == list.size()) {
            // il faut reset le tableau
            double totalTmp = getSum(list);
            initWithZero(list, HISTORY_SIZE);
            list.set(0, totalTmp);
            currentIndex = 1;
            addTransactionToList(list, value);
        } else {
            // il faut ajouter le credit au current index
            list.set(currentIndex++, value);
        }
    }

    public double getSolde() {
        return getCredit() - getDebit();
    }

    public List<Double> getCreditHistory() {
        return this.creditHistory;
    }
}
