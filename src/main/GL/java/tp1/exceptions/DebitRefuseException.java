package main.GL.java.tp1.exceptions;

public class DebitRefuseException extends Exception {

    public DebitRefuseException() {
        super("Le debit n'a pas pu être accordé");
    }
}
