package main.GL.java.tp1.exceptions;

public class CompteInexistantException extends Exception {
    public CompteInexistantException() {
        super("Le compte est inexistant");
    }
}
