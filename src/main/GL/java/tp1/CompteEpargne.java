package main.GL.java.tp1;

import main.GL.java.tp1.exceptions.DebitRefuseException;

public class CompteEpargne extends Compte {

    private float interet = 0.02f;

    public float getInteret() {
        return interet;
    }

    public void echeance() {
        credite(getSolde()*getInteret());
    }

    @Override
    public void debite(double debit) throws DebitRefuseException {
        if(debit > getSolde()) throw new DebitRefuseException();
        super.debite(debit);
    }
}
