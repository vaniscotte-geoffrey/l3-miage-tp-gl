package main.GL.java.tp1;


import main.GL.java.tp1.exceptions.CompteInexistantException;
import main.GL.java.tp1.exceptions.DebitRefuseException;

import java.util.ArrayList;
import java.util.List;

public class Banque {
    public List<Compte> comptes;

    public Banque() {
        this.comptes = new ArrayList<>();
    }

    public double getTotalCompteSoldes() {
        return comptes.stream().mapToDouble(Compte::getSolde).sum();
    }

    public Compte getCompte(int numero) throws CompteInexistantException {
        if (comptes.size() <= numero || comptes.get(numero) == null) throw new CompteInexistantException();
        return comptes.get(numero);
    }

    public void ouvrirCompte(Compte compte) {
        comptes.add(compte);
    }

    public void credite(int numero, double credit) throws CompteInexistantException {
        getCompte(numero).credite(credit);
    }

    public void debite(int numero, double debit) throws DebitRefuseException, CompteInexistantException {
        getCompte(numero).debite(debit);
    }

    public void virement(int source, int destination, double montant) throws CompteInexistantException, DebitRefuseException {
        getCompte(source).debite(montant);
        getCompte(destination).credite(montant);
    }
}
